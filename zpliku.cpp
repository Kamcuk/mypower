#include "zpliku.h"
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <streambuf>
#include <unistd.h>
#include <algorithm>
#include <vector>
#include <sstream>
#include <fstream>

std::vector<std::string> services;
std::vector<bool> running;

std::vector<std::string> ZPliku::getServices(std::string script)
{
    std::vector<std::string> ret;
    char buf[512]; /* endless ! */
    FILE *fp;
    std::string show = script + " show | sed 's/services: //'";
    const char delim = ' ';

    fp = popen(show.c_str(), "r");
    if (fp == NULL) {
        printf("Alloc_dynamic_menus() Failed to run command\n" );
        return ret;
    }
    while( fgets(buf, sizeof(buf), fp) != NULL ) {
        std::istringstream buff(buf);
        std::string temp;
        while( std::getline(buff, temp, delim) ) {
            if ( temp[temp.size() - 1] == '\n' ) {
                temp = temp.substr(0, temp.size()-2);
            }
            ret.push_back(temp);
        }
    }
    pclose(fp);

    updateRunning(script);

    services = ret;

    return ret;
}

void ZPliku::updateRunning(std::string script)
{
    std::vector<bool> ret;
    char buf[512]; /* endless ! */
    FILE *fp;
    std::string show = script + " check ";

    fp = popen(show.c_str(), "r");
    if (fp == NULL) {
        printf("Alloc_dynamic_menus() Failed to run command\n" );
        return;
    }
    while( fgets(buf, sizeof(buf), fp) != NULL ) {
        std::istringstream buff(buf);
        std::string temp;
        while( std::getline(buff, temp) ) {
            std::string strnum = temp.substr(temp.size()-1, temp.size()-1);
            ret.push_back( strnum[0] == '1' );
        }
    }
    pclose(fp);

    running = ret;
}

bool ZPliku::getRunning(std::string serv)
{
    std::vector<std::string>::iterator it = std::find(services.begin(), services.end(), serv);
    if ( it == services.end() )
        return 0;
    int numer = std::distance(services.begin(), it);
    return running[numer];
}

std::string ZPliku::zmienStan(std::string script, std::string serv)
{
    std::string ret;
    std::string append;
    if ( getRunning(serv) ) {
        append = " stop ";
    } else {
        append = " start ";
    }
    char buf[512]; /* endless ! */
    FILE *fp;
    std::string show = script +  append + " " + serv;

    fp = popen(show.c_str(), "r");
    if (fp == NULL) {
        return "Alloc_dynamic_menus() Failed to run command\n";
    }
    while( fgets(buf, sizeof(buf), fp) != NULL ) {
        ret = ret + buf;
    }
    pclose(fp);

    return ret;
}

unsigned long ZPliku::getMaxBrightness()
{
    std::fstream ffile;
    unsigned long ret;
    ffile.open("//sys//class//backlight//intel_backlight//max_brightness", std::ios_base::out);
    ffile >> ret;
    ffile.close();
    return ret;
}

unsigned long ZPliku::getBrightness()
{
    std::fstream ffile;
    unsigned long ret;
    ffile.open("//sys//class//backlight//intel_backlight//brightness", std::ios_base::out);
    ffile >> ret;
    ffile.close();
    return ret;
}

void ZPliku::setBrightness(unsigned long set)
{
    std::fstream ffile;
    ffile.open("//sys//class//backlight//intel_backlight//brightness", std::ios_base::in);
    ffile << set;
    ffile.close();
}

void ZPliku::writeToFile(unsigned long what, std::string where)
{
    std::fstream ffile;
    ffile.open(where, std::ios_base::in);
    ffile << what;
    ffile.close();
}
