#ifndef ZPLIKU_H
#define ZPLIKU_H
#include <vector>
#include <string>

namespace ZPliku
{
    std::vector<std::string> getServices(std::string PSSCRIPT);
    void updateRunning(std::string script);
    bool getRunning(std::string serv);
    std::string zmienStan(std::string script, std::string serv);

    unsigned long getMaxBrightness();
    unsigned long getBrightness();
    void setBrightness(unsigned long set);
    void writeToFile(unsigned long what, std::string where);
};

#endif // ZPLIKU_H
