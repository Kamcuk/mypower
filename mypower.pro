#-------------------------------------------------
#
# Project created by QtCreator 2015-06-30T15:43:07
#
#-------------------------------------------------

QT       += core gui
QMAKE_CXXFLAGS += -std=c++11
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = mypower
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    zpliku.cpp

HEADERS  += mainwindow.h \
    zpliku.h

FORMS    += mainwindow.ui
