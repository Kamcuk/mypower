#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPushButton>
#include <iostream>
#include <zpliku.h>
#include <QScrollBar>
using namespace std;

#define BRIGHTNESS_INTEL_FILE "/sys/class/backlight/intel_backlight/brightness"
#define BRIGHTNESS_ACPI_FILE "/sys/class/backlight/acpi_video0/brightness"

const unsigned long scale = 100000;

MainWindow::MainWindow(std::string psscript, QWidget *parent) :
    QMainWindow(parent),
    psscript(psscript),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->menuCpu, SIGNAL(aboutToShow()), this, SLOT(on_aboutToShow_menu_Cpu()));
    connect(ui->menuPss_services, SIGNAL(aboutToShow()), this, SLOT(on_aboutToShow_menu_Pss_services()));

    std::vector<std::string> sru = ZPliku::getServices(psscript);
    for(unsigned int i=0;i<sru.size();i++) {
        std::string serv = sru[i];

        QAction *action = ui->menuPss_services->addAction(QString::fromStdString(serv));
        action->setCheckable(true);
        action->setObjectName(QString::fromStdString(serv));
        connect(action, SIGNAL(triggered()), this, SLOT(buttClicked()));
    }

    ui->verticalSlider->setRange(0, ZPliku::getMaxBrightness()/scale);
    ui->verticalSlider->setValue(ZPliku::getBrightness()/scale);
}

MainWindow::~MainWindow()
{
    for(QPushButton *butt:buttony) {
        delete butt;
    }
    delete ui;
}

void MainWindow::on_aboutToShow_menu_Cpu()
{
    std::string ret = RunGrabOutput("cpupower frequency-info");
    ui->actionOndemand->setChecked(
                ret.find("The governor \"ondemand\" may") != ret.npos );
    ui->actionPerformance->setChecked(
                ret.find("The governor \"performance\" may") != ret.npos );
    ui->actionUserspace->setChecked(
                ret.find("The governor \"userspace\" may") != ret.npos );
    ui->action600->setChecked(
                ret.find("CPU frequency is 600 MHz") != ret.npos );
    ui->action1200->setChecked(
                ret.find("CPU frequency is 1.20 GHz") != ret.npos );
}


void MainWindow::on_aboutToShow_menu_Pss_services()
{
    std::vector<std::string> sru = ZPliku::getServices(psscript);
    QList<QAction*> actions = ui->menuPss_services->actions();
    for(unsigned int i=0;i<sru.size();i++) {
        std::string serv = sru[i];
        actions[i]->setChecked(ZPliku::getRunning(serv));
    }

}

void MainWindow::paintEvent(QPaintEvent *)
{
}

void MainWindow::ShowText(std::string str)
{
    QTextBrowser *textbrowser = ui->textBrowser_2;
    textbrowser->setText(ui->textBrowser_2->toPlainText()
                         +tr("----------------------------------\n")
        +QString::fromStdString(str));
    QScrollBar *sb = textbrowser->verticalScrollBar();
    sb->setValue(sb->maximum());
}
void MainWindow::ShowText(QString str)
{
    return this->ShowText((std::string)str.toStdString());
}

void MainWindow::buttClicked()
{
    std::string serv =	sender()->objectName().toStdString().c_str();
    ShowText(serv+" : "+ZPliku::zmienStan(psscript, serv));
}


void MainWindow::on_verticalSlider_actionTriggered(int)
{
    //unsigned long value = ui->verticalSlider->value()*scale;
    ShowText("Brigthness set : "+std::to_string(ui->verticalSlider->value())+"\n");
    //ZPliku::setBrightness(value);
    //this->update();
}

std::string MainWindow::RunGrabOutput(std::string path)
{
    FILE *fp;
    char buff[1024];
    std::string ret;

    /* Open the command for reading. */
    path = path + " 2>&1 ";
    ret.append("# " + path + "\n");
    fp = popen(path.c_str(), "r");
    if (fp == NULL) {
      return "Failed to run command\n";
    }

    /* Read the output a line at a time - output it. */
    while (fgets(buff, sizeof(buff)-1, fp) != NULL) {
      ret.append(buff);
    }

    /* close */
    pclose(fp);

    return ret;
}


void MainWindow::on_action600_triggered()
{
    ShowText(RunGrabOutput("gksu \"cpupower frequency-set -f 600\""));
}

void MainWindow::on_action1200_triggered()
{
     ShowText(RunGrabOutput("gksu \"cpupower frequency-set -f 1200\""));
}

void MainWindow::on_actionPerformance_triggered()
{
    ShowText(RunGrabOutput("gksu \"cpupower frequency-set -g performance\""));
}

void MainWindow::on_actionOndemand_triggered()
{
    ShowText(RunGrabOutput("gksu \"cpupower frequency-set -g ondemand\""));
}

void MainWindow::on_actionPrint_info_triggered()
{
    ShowText(RunGrabOutput("gksu \"cpupower frequency-info\""));
}

void MainWindow::on_verticalSlider_2_actionTriggered(int action)
{
    float var = (float)ui->verticalSlider_2->value()/100.f;
    ShowText(RunGrabOutput("xrandr --output LVDS1 --brightness " + std::to_string(var) ));
}
