#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <iostream>
#include <vector>
#include <string>
#include <QPushButton>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    std::string psscript;
    std::vector<QPushButton *> buttony;

public:
    explicit MainWindow(std::string psscript, QWidget *parent = 0);
    ~MainWindow();
    void ShowText(QString str);
    void ShowText(std::string str);
    std::string RunGrabOutput(std::string path);
public slots:
    void buttClicked();
    void paintEvent(QPaintEvent *event);
    void on_aboutToShow_menu_Cpu();
    void on_aboutToShow_menu_Pss_services();
private slots:
    void on_verticalSlider_actionTriggered(int action);

    void on_action600_triggered();

    void on_action1200_triggered();

    void on_actionPerformance_triggered();

    void on_actionOndemand_triggered();

    void on_actionPrint_info_triggered();

    void on_verticalSlider_2_actionTriggered(int action);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
